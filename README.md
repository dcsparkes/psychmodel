# psychmodel

A Python library providing a nontrivial humanoid psychological model based around values, reactions, ambitions and goals.

# Why do this?

While playing games where characters co-operate, it bugged me that the model by which most of these characters interact is too simplistic. You can make people who hate you, like you, just by repeatedly doing small favours for them. You can give people gifts until they fall in love with you.

Good drama comes from conflict, including internal conflict. The aim of this project is to try and create a system whereby characters can have complex relationships.

I've had a look around and not found any code which does anything like this, nor any existing models I could adapt (or even ones I couldn't because of license concerns), so I'm very open to feedback and input.

# How do this?

That is a very good question. I am starting from scratch on this, but I expect the project will look like:

1. User Stories
1. Design documentation; nomenclature:
    1. A Model is a representation of values and personal agenda
    1. A Player is somebody, e.g. a human playing the game, interacting with Characters
    1. A Character has a psychological Model based around their Values
    1. An Agent is a catch-all term for Players and Characters, i.e. entities which can act in the world
    1. An Event is created by an Agent; it is witnessed by Characters who may react with their own Events
1. Implementation
    1. Enumerate Values for the Model
1. Unit tests
    1. These will create characters with specific relationships to each other, inject events and see where we end up
    1. These will include tests against anti-features, e.g. bribing somebody from hate into love

# Who do this?

See the git commit history for details, but this is starting as a personal project by Dave Page.
